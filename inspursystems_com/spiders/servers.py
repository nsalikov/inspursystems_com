# -*- coding: utf-8 -*-
import re
import scrapy
from scrapy.spiders import SitemapSpider


class ServersSpider(SitemapSpider):
    name = 'servers'
    allowed_domains = ['inspursystems.com']

    sitemap_urls = ['https://www.inspursystems.com/product-sitemap.xml']
    sitemap_rules = [('/product/', 'parse')]


    def parse(self, response):
        d = {}

        d['url'] = response.url

        d['server'] = ' '.join(filter(None, [t.strip() for t in response.css('h1 ::text').extract()]))
        if not d['server']:
            d['server'] = ' '.join(filter(None, [t.strip() for t in response.css('.product-num ::text').extract()]))

        d['datasheet'] = _strip(response.xpath('//a[contains(@class, "btn-primary") and (contains(text(), "Datasheet") or contains(text(), "Data Sheet"))]/@href').extract_first(), default='')
        if not d['datasheet']:
            d['datasheet'] = _strip(response.xpath('//a[descendant::img[contains(@src, "datasheet-button")]]/@href').extract_first(), default='')
        if not d['datasheet']:
            d['datasheet'] = _strip(response.xpath('//*[@class="resources-box"]//a[contains(text(), "Datasheet") or contains(text(), "Data Sheet")]/@href').extract_first(), default='')

        if d['datasheet'] and not d['datasheet'].startswith('http'):
            d['datasheet'] = response.urljoin(d['datasheet'])

        if d['datasheet']:
            d['datasheet'] = d['datasheet'].replace('http://', 'https://')

        d['specs'] = dict(self.parse_specs(response))

        return d


    def parse_specs(self, response):
        for tr in response.css('.product-specs tr'):
            if tr.css('th') and tr.css('td'):
                key = ' '.join(filter(None, [t.replace('\xa0', ' ').strip() for t in tr.css('th ::text').extract()]))
                value = list(filter(None, [t.replace('\xa0', ' ').strip() for t in tr.css('td ::text').extract()]))
            elif tr.css('td:nth-child(1)') and tr.css('td:nth-child(2)'):
                key = ' '.join(filter(None, [t.replace('\xa0', ' ').strip() for t in tr.css('td:nth-child(1) ::text').extract()]))
                value = list(filter(None, [t.replace('\xa0', ' ').strip() for t in tr.css('td:nth-child(2) ::text').extract()]))
            else:
                key = None
                value = None

            if key and value:
                yield (key, value)


def _strip(string, default=None):
    if string:
        return string.strip()
    return default
